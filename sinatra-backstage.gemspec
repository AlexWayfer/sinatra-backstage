Gem::Specification.new do |s|
	s.name          = 'sinatra-backstage'
	s.version       = '0.2.9.3'
	s.date          = Date.today.to_s
	s.summary       = 'Extension for Sinatra and DataMapper'
	s.description   = 'Sinatra-Backstage simplifies the use' \
	                  ' of the basic routes and models for Sinatra' \
					  ' and DataMapper. Currently it includes REST and User.'
	s.authors       = ['Alexander Popov']
	s.email         = 'alex.wayfer@gmail.com'
	s.files	= [
		'lib/sinatra/backstage.rb',
		'lib/sinatra/backstage/rest.rb',
		'lib/sinatra/backstage/user.rb',
		'lib/sinatra/backstage/stored_file.rb',
		'lib/sinatra/backstage/rest/rest_model.rb',
		'lib/sinatra/backstage/rest/rest_routes.rb',
		'lib/sinatra/backstage/user/user_helper.rb',
		'lib/sinatra/backstage/user/user_model.rb',
		'lib/sinatra/backstage/user/user_middleware.rb',
		'lib/sinatra/backstage/stored_file/stored_file_helper.rb',
		'lib/sinatra/backstage/stored_file/stored_file_model.rb',
		'lib/sinatra/backstage/stored_file/stored_file_routes.rb'
	]
	s.homepage      = 'https://bitbucket.org/AlexWayfer/sinatra-backstage'
	s.license       = 'MIT'

	s.add_runtime_dependency 'gorilla-patch', '>= 0.0.4'
	# s.add_runtime_dependency 'sinatra', '>= 1.2'
	# s.add_runtime_dependency 'sinatra-contrib', '>= 1.2'
	# s.add_runtime_dependency 'sinatra-conditions', '~> 0'
	# s.add_runtime_dependency 'dm-core', '>= 1.2'
	# s.add_runtime_dependency 'dm-types', '>= 1.2'
	# s.add_runtime_dependency 'dm-validations', '>= 1.2'
	# s.add_runtime_dependency 'dm-timestamps', '>= 1.2'
	# s.add_runtime_dependency 'dm-serializer', '>= 1.2'
end
