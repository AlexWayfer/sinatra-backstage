## database gems
require 'dm-core'
require 'dm-types'
require 'dm-validations'
require 'dm-timestamps'

## Stored File class (DataMapper Resource)
module Sinatra
	module Backstage
		module StoredFile
			class StoredFile

				URL_DIR = "/files"
				DIR = "/public"

				include DataMapper::Resource

				## Properties
				property :id, Serial
				property :filename, String, :required => true
				property :filesize, Integer, :required => true, :default => 0
				property :created_at, DateTime, :default => DateTime.now

				default_scope(:default).update(:order => [:created_at.desc])

				def filesize_kb
					"#{filesize/1024}"
				end

				def uploaded
					"#{created_at.strftime('%d %b')}"
				end

				def self.stored_dir
					"#{self::DIR + self::URL_DIR}"
				end

				def stored_filename
					extname = File.extname(filename)
					"#{File.basename(filename, extname)}_#{id}#{extname}"
				end

				def stored_path
					".#{self.class.stored_dir}/#{stored_filename}"
				end

				def url
					"#{self.class::URL_DIR}/#{stored_filename}"
				end

				## Methods
				def self.create(attrs, tempfile, extra = {})
					file = super( attrs.merge extra )
					write_file(file, tempfile)
					file
				rescue DataMapper::SaveFailureError => e
					puts e.resource.errors.inspect
				end

				def update(attrs, tempfile, extra = {})
					delete_file
					super( attrs.merge extra )
					self.class.write_file(self, tempfile)
				rescue DataMapper::SaveFailureError => e
					puts e.resource.errors.inspect
				end

				## Hooks
				before :destroy do
					delete_file
				end


			protected

				## Helpers
				def self.write_file(stored_file, tempfile)
					FileUtils.mkdir_p ".#{stored_dir}" unless Dir.exist? ".#{stored_dir}"
					File.open(stored_file.stored_path, 'wb') do |f|
						f.write(tempfile.read)
					end
				end

				def delete_file
					File.delete(stored_path) if File.exist?(stored_path)
				end

			end
		end
	end
end