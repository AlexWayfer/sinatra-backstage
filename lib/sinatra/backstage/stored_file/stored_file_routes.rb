module Sinatra
	module Backstage
		module StoredFile
			module Routing

				def self.included(controller)
					controller.extend self
				end

				def receive_attachment(klass, child_klass, namespace, field_name)

					helpers Sinatra::Backstage::StoredFile::Helper

					## Add logo after create franchise
					before "#{namespace}/new", :method => :post do
						if params[:object][field_name]
							begin
								# puts "-- StoredFile before new ( params[:object] = #{params[:object]}"
								attachment = params[:object].delete(field_name)
								attachment_attrs = get_attachment_attrs(attachment)
								# puts "-- StoredFile before new ( attachment_attrs = #{attachment_attrs}"
								params[:object][field_name] = child_klass.create(
									attachment_attrs,
									attachment[:tempfile]
								)
								# puts "-- StoredFile before new ( result params[:object] = #{params[:object]}"
							rescue DataMapper::SaveFailureError => e
								puts e.resource.errors.inspect
							end
						end
					end

					## Create logo object before update franchise
					before "#{namespace}/:id", :method => :put do |id|
						if params[:object][field_name]
							begin
								# puts "-- StoredFile before put ( params[:object] = #{params[:object]}"
								attachment = params[:object].delete(field_name)
								attachment_attrs = get_attachment_attrs(attachment)
								child_obj = klass.get!(id).send(field_name)
								if child_obj.nil?
									params[:object][field_name] = child_klass.create(
										attachment_attrs,
										attachment[:tempfile]
									)
								else
									child_obj.update(
										attachment_attrs,
										attachment[:tempfile]
									)
								end
							rescue DataMapper::SaveFailureError => e
								puts e.resource.errors.inspect
							end
						end
					end

					## Destroy file object
					delete "#{namespace}/:id/#{field_name}" do |id|
						child_obj = klass.get!(id).send(field_name)
						child_obj.destroy unless child_obj.nil?
						redirect "#{namespace}/#{id}"
					end

				end

			end

			def self.included(app)
				app.include Routing
			end

			def self.registered(app)
				app.register Routing
			end

		end
	end
end