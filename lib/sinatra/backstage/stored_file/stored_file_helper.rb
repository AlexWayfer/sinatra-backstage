module Sinatra
	module Backstage
		module StoredFile
			module Helper

				def get_attachment_attrs(attachment)
					# puts "-- Backstage::StoredFile::Helper get_attachment_attrs ( attachment = #{attachment} )"
					{
						:filename => attachment[:filename],
						:filesize => File.size(attachment[:tempfile].path)
					}
				end

			end

			include Helper
		end
	end
end