require_relative './user_helper'

module Sinatra
	module Backstage
		module User

			def self.registered(app)

				## Configure
				app.enable :sessions
				app.set :user_class, Sinatra::Backstage::User::User

				## Helpers
				app.helpers Sinatra::Cookies,
							Sinatra::Backstage::User::Helper

				## Routes
				### get login page
				app.get '/login' do
					eval "#{settings.render_engine} 'user/login'"
				end

				app.get '/signup' do
					eval "#{settings.render_engine} 'user/signup'"
				end

				### signup user
				app.post '/signup' do
					# puts "params = #{params}"
					# halt
					@errors = {}
					begin
						user = settings.user_class.create params[:user]
						set_session user
						redirect '/'
					rescue DataMapper::SaveFailureError => e
						# puts e.resource.errors.inspect
						# puts "-- Backstage::UserMiddleware /signup rescue ( e.resource.errors.keys = #{e.resource.errors.keys} )"
						# puts "-- Backstage::UserMiddleware /signup rescue ( e.resource.errors.to_hash = #{e.resource.errors.to_hash} )"
						e.resource.errors.to_hash.each do |key, error|
							# puts "-- Backstage::UserMiddleware /signup rescue each ( key = #{key} )"
							# puts "-- Backstage::UserMiddleware /signup rescue each ( error[0].to_s = #{error[0].to_s} )"
							@errors[key.to_s] = {
								'text' => error[0].to_s,
								'autofocus' => true
							}
						end
						# @errors.flatten!
						# puts @errors.inspect
						params[:user].delete_if do |key, val|
							['password', 'password_confirmation'].include? key
						end
					end
					# puts "User::Middleware post /signup ( params['user']['email'] = #{params['user']['email']} )"
					eval "#{settings.render_engine} 'user/signup'"
				end

				### login user
				app.post '/login' do
					@errors = {}
					password = params[:user].delete('password')
					user = settings.user_class.first params[:user]
					# puts "User::Middleware post '/login' ( params[:user] = #{params[:user]} )"
					# puts "User::Middleware post '/login' ( user = #{user} )"
					ident_field = params[:user].keys.first
					if user.nil?
						@errors[ident_field] = {
							'text' => "Incorrect #{params[:user].keys.join(' or ')}",
							'autofocus' => true
						}
					else
						if user.password == password
							set_session user
							redirect '/'
						else
							@errors['password'] = {
								'text' => "Incorrect password",
								'autofocus' => true
							}
						end
					end
					eval "#{settings.render_engine} 'user/login'"
				end

				### logout user
				app.get '/logout' do
					set_session nil
					redirect '/'
				end

			end
		end
	end
end