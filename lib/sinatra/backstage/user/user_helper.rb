## gorilla-patch
require 'gorilla-patch'

## sinatra gems
require 'sinatra'

require_relative './user_model'

module Sinatra
	module Backstage
		module User
			module Helper

				using GorillaPatch::ArrayExt

				def self.included(app)
					if app.ancestors.include? Sinatra::Base
						# app.set(:user_access) do |*flags|
						# 	condition do
						# 		# roles = settings.user_class.roles - [authorized_user.role] if roles == [:not_allowed]
						# 		roles = flags + (flags.include?(:not_allowed) ? [authorized_user.role] : [])
						# 		# puts "-- Backstage::UserHelper :user_access ( roles = #{roles} )"
						# 		settings.site_routes.each do |controller, hash|
						# 			if request.path =~ /^(#{hash['href']}$|#{hash['href']}\/.*$)/
						# 				# puts "-- Backstage::UserHelper :user_access each ( controller = #{controller} )"
						# 				# puts "-- Backstage::UserHelper :user_access each ( hash = #{hash} )"
						# 				# puts "-- Backstage::UserHelper :user_access each ( roles = #{roles} )"
						# 				# puts "-- Backstage::UserHelper :user_access ( hash['access'].include_any?(roles) = #{hash['access'].include_any?(roles)} )"
						# 				# puts "-- Backstage::UserHelper :user_access each ( !hash['access'].include_any?(roles) && roles.include?(:not_allowed) = #{!hash['access'].include_any?(roles) && roles.include?(:not_allowed)} )"
						# 				return !hash['access'].include_any?(roles) && roles.include?(:not_allowed)
						# 			end
						# 		end
						# 	end
						# end

						## Hooks
						# app.before :user_access => [:anon] do
						# 	# puts "-- Backstage::User before authorized? = #{authorized?}"
						# 	redirect '/' if authorized?
						# 	# settings.site_routes[request.path]['active'] = true if settings.site_routes[request.path]
						# end

						# app.after :user_access => [:anon] do
						# 	settings.site_routes[request.path]['active'] = false if settings.site_routes[request.path]
						# end
					end
				end

				def set_session(user)
					if user
						cookies[:username] = user.username
						cookies[:session] = user.session
					else
						cookies[:username] = nil
						cookies[:session] = nil
					end
				end

				def authorized?
					# puts "-- Backstage::UserHelper authorized? ( authorized_user.role = #{authorized_user.role} )"
					authorized_user.role != :anon
				end

				def authorized_user
					if @authorized_user.nil?
						if cookies[:username] && cookies[:session]
							user = settings.user_class.first :username => cookies[:username]
							if user && user.session == cookies[:session]
								return @authorized_user = user
							end
						end
						@authorized_user = settings.user_class.new
					end
					# puts "-- Backstage::UserHelper authorized_user ( @authorized_user.inspect = #{@authorized_user.inspect} )"
					# puts "-- Backstage::UserHelper authorized_user ( caller = #{caller} )"
					@authorized_user
				end

			end
		end
	end
end
