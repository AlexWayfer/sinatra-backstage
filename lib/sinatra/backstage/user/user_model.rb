## database gems
require 'dm-core'
require 'dm-types'
require 'dm-validations'

## Store class (DataMapper Resource)
module Sinatra
	module Backstage
		module User
			class User

				include DataMapper::Resource

				## Properties
				property :id, Serial
				property :username, String
				property :password, BCryptHash,
					:required => true
				def password=(new_password)
					# puts "-- Backstage::User::Model password= ( new_password = #{new_password} )"
					super new_password unless new_password.empty?
				end
				# property :salt, BCryptHash,
				# 	:required => true,
				# 	:default => lambda {|r, p| BCrypt::Engine.generate_salt }

				# def salt
				# 	self.salt = BCrypt::Engine.generate_salt if super.nil?
				# end

				# def password=(new_password)
				# 	# self.password_hash = generate_salt_hash(new_password)
				# 	self.password_hash = new_password
				# end
				attr_accessor :role, :status
				# ROLES = Enum[:anon, :user]
				# property :role, self::ROLES,
				# 	:required => true,
				# 	:default => :anon

				def session
					# generate_salt_hash(username)
					BCrypt::Engine.hash_secret(username, password)
				end

				## Methods
				# def generate_salt_hash(check_password)
				# 	BCrypt::Engine.hash_secret(check_password, salt)
				# end

				def self.roles
					# puts "-- Backstage::User::Model roles ( role.options = #{role.options} )"
					role.options[:flags]
				end

				def allowed_roles
					self.class.roles[1..self.class.roles.index(role)]
				end

				def allowed_editing_roles
					allowed_roles - [role]
				end

				def role_allowed?(other_role)
					allowed_roles.include?(other_role.to_sym)
				end

				def role_editing_allowed?(other_user)
					allowed_editing_roles.include?(other_user.role)
				end

				def user_editing_allowed?(other_user)
					role_editing_allowed?(other_user) || id == other_user.id
				end

			end
		end
	end
end
