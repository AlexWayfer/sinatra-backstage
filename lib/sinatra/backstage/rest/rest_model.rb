## Rest class (DataMapper Resource)
module Sinatra
	module Backstage
		module REST
			module Model

				def form_update
					"#{form_id}-update"
				end

				def form_delete
					"#{form_id}-delete"
				end

			end
		end
	end
end