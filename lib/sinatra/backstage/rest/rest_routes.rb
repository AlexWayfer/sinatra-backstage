## gorilla-patch
require 'gorilla-patch'

## datamapper
require 'dm-serializer'

module Sinatra
	module Backstage
		module REST
			module Routing

				using GorillaPatch::ArrayExt
				using GorillaPatch::HashExt
				using GorillaPatch::ModuleExt

				def self.included(controller)
					controller.extend self
				end

				def rest_routes(klass, namespace, display_list = false)

					## Routes
					### get list
					get namespace do
						## get format
						# puts "-- REST.Routing get namespace ( params = #{params})"
						# puts "-- REST.Routing get namespace ( request['params'] = #{request['params']})"
						format = params.delete('format') || 'html'
						# puts "-- REST.Routing get namespace after format ( params = #{params})"
						format = 'json' if request.accept.include_any?('text/json', 'application/json')
						## get json options
						json_method = params.delete('json_method')
						json_opts = { methods: ([json_method] || []) }
						## get objects
						@objects = rest_get_all klass, params
						# puts "-- REST.Routing get namespace ( @objects = #{@objects})"
						# puts "-- REST.Routing get namespace ( @objects.to_a = #{@objects.to_a})"
						# puts "-- REST.Routing get namespace ( @objects.to_json = #{@objects.to_json})"
						# puts "-- REST.Routing get namespace ( @objects.to_a.to_json = #{@objects.to_a.to_json})"
						## return
						case format
						when 'html'
							halt eval "#{settings.render_engine} '#{klass.demodulize.underscore}/list'"
						when 'json'
							halt @objects.to_json(json_opts)
						end
						error 406
					end

					### get add-form
					get "#{namespace}/new" do
						template = ["#{klass.demodulize.underscore}/new"]
						if display_list
							@objects = rest_get_all klass, params
							template.unshift "#{klass.demodulize.underscore}/list"
						end
						eval "#{settings.render_engine} ['#{template.join('\',\'')}']"
					end

					### create object
					post "#{namespace}/new" do
						# puts "params = #{params}"
						# puts "params[:object] = #{params[:object]}"
						# puts "request['params'] = #{request['params']}"
						# puts "@params = #{@params}"
						# puts "-- rest_routes post namespace = #{namespace}"
						# puts "-- rest_routes post klass = #{klass}"
						# halt
						@errors = {}
						begin
							object = klass.create params[:object]
							redirect "#{namespace}/#{object.id}"
						rescue DataMapper::SaveFailureError => e
							puts e.resource.errors.inspect
							e.resource.errors.to_hash.each do |key, error|
								# puts "-- REST.Routing new ( error = #{error} )"
								@errors[key.to_s] = {
									'text' => error[0].to_s,
									'autofocus' => true
								}
							end
							# @errors.flatten!
							template = ["#{klass.demodulize.underscore}/new"]
							if display_list
								@objects = rest_get_all klass, params
								template.unshift "#{klass.demodulize.underscore}/list"
							end
							eval "#{settings.render_engine} ['#{template.join('\',\'')}']"
						end
					end

					get "#{namespace}/:id" do |id|
						@object = klass.get(id)
						halt 404 if @object.nil?
						template = ["#{klass.demodulize.underscore}/edit"]
						if display_list
							@objects = rest_get_all(klass)
							template.unshift "#{klass.demodulize.underscore}/list"
						end
						eval "#{settings.render_engine} ['#{template.join('\',\'')}']"
					end

					### update object
					put "#{namespace}/:id" do |id|
						@errors = {}
						begin
							@object = klass.get!(id)
							@object.update params[:object]
							redirect (display_list ? "#{namespace}/#{id}" : namespace)
						rescue DataMapper::SaveFailureError => e
							e.resource.errors.to_hash.each do |key, error|
								# puts "-- REST.Routing new ( error = #{error} )"
								@errors[key.to_s] = {
									'text' => error[0].to_s,
									'autofocus' => true
								}
							end
							# @errors.flatten!
							template = ["#{klass.demodulize.underscore}/edit"]
							if display_list
								@objects = rest_get_all(klass)
								template.unshift "#{klass.demodulize.underscore}/list"
							end
							eval "#{settings.render_engine} ['#{template.join('\',\'')}']"
						end
					end

					### delete object
					delete "#{namespace}/:id" do |id|
						klass.get!(id).destroy
						redirect namespace
					end

				end

				def rest_get_all(klass, params = {})
					params.merge!(request['params']) if request['params']
					klass.all params.except('object').keys_to_sym
				end

			end

			def self.included(app)
				app.include Routing
			end

		end
	end
end
